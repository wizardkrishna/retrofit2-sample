package com.iayon.retrofit20tutorial;

import models.GitResult;
import rest.RestClient;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by krishnaraj on 24/3/16.
 */
public class MyService {
    RestClient.GitApiInterface service = RestClient.getClient();
    String query;
    Observable<GitResult> resultObservable;

    public Observable<GitResult> getUsersByName(String query) {
        this.query = query;
        resultObservable = service.getUsersByName(query, 0).cache();
        return resultObservable;
    }

    public Observable<GitResult> searchNext() {
        Observable nextResultObservable = resultObservable
                .flatMap(new Func1<GitResult, Observable<?>>() {
                    @Override
                    public Observable<?> call(GitResult gitResult) {
                        return service.getUsersByName(query, gitResult.getNextPage());
                    }
                });

        return nextResultObservable;
    }
}
